package com.example.sukangoding.materialtab.fragment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.sukangoding.materialtab.R;

public class Sub1Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub1);
    }
}
